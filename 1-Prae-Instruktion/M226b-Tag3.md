# Präinstruktion M226b Tag 3

## Spielregel
	Beantworten Sie *schriftlich* und alleine für sich,
	die folgenden Fragen. Recherchieren Sie vorerst nicht!
	Vermutungen sind auch gut.
	
	Nach 15 min. können Sie im Internet
	oder in den Unterlagen recherchieren.
	Das Gespräch (Murmelrunde) mit dem/den
	Nachbarn ist in dieser Phase gut, hilfreich und erwünscht!

## Fragen
- Was bedeutet "Referenz"?
- Was bedeutet Kapselung (encapsulation)?
	<br>(Gressli, S. 13-14)
- Wo und wie kann man eine Kapselung erkennen, wo kommt das vor?
- Was bedeutet, wie wirkt "call by refenence" und "call by value"?
	<br>(Gressli, S. 28-30)
- Machen Sie 3 einfache textuelle Beispiele für eine "has a"-Beziehung 
- Was hat die "has a"-Beziegung mit einer Aggregation und einer Komposition zu tun?
	<br>(Gressli, S. 39-40)

	
[Gressli](../2-Unterlagen/00-Buecher/Buch__P.Gressli_Objekte_und_Klassen.pdf)
