import java.util.Scanner;
import java.util.ArrayList;

public class aufgabeeins {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> eingaben = new ArrayList<>();
        
        for (int i = 1; i <= 10; i++) {
            System.out.print("Bitte geben Sie etwas ein (Eingabe " + i + "/10): ");
            String eingabe = scanner.nextLine();
            if (eingabe.equalsIgnoreCase("Stop")) {
                break;
            }
            
            eingaben.add(eingabe);
        }
        
        System.out.println("\nEingegebene Werte:");
        for (String eingabe : eingaben) {
            System.out.println(eingabe);
        }
        
        scanner.close();
    }
}