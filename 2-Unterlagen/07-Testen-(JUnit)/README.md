## JUnit Tests

[**Einfuehrung-in-JUnit-Tests.pdf**](Einfuehrung-in-JUnit-Tests.pdf)

[M226_JUnit_Eclipse_1.0.pdf](M226_JUnit_Eclipse_1.0.pdf)

[testing-tutorial.pdf](testing-tutorial.pdf)

**<https://junit.org/junit5/docs/current/user-guide>**

## Aufgabe

Zeitbedarf ca. 45 min


**Lesen Sie zuerst das PDF und studieren Sie den User-Guide.**

Erstellen Sie (jede:r) dann eine Klasse "Geometrie" in der es eine Methode 
- getRechteckFlaeche( int laenge, int breite)

und eine Methode 

- getRechteckUmfang( int laenge, int breite) 

gibt und erstellen Sie dann **mindestens 3 JUnit-Tests** dazu 
<br>
(OK-Fälle und NotOK-Fälle wie 0-Werte und Minuswerte). 


Zeigen Sie das Resultat der Lehrperson und helfen Sie anschliessend den anderen Klassenkamerad:innen. 
Ziel ist es, das zum angegebenen Zeitpunkt alle ihre JUnit-Test gemacht und verstanden haben.