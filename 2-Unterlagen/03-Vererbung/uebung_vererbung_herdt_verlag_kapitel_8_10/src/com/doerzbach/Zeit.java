package com.doerzbach;

import java.lang.reflect.InvocationTargetException;

public class Zeit {
    private int hours;
    private int minutes;

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }
    public Zeit clone() {
        Zeit clonedobject=new Zeit();
        clonedobject.hours = this.hours;
        clonedobject.minutes = this.minutes;
        return clonedobject;
    }
    public boolean equals(Zeit zeit){
        return zeit.minutes==this.minutes&&zeit.hours==this.hours;
    }
}
