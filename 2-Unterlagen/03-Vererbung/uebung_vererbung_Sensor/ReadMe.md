Sensor Exercise
===============

This code is a solution of the Sensor Exercise.


Exercise on Inheritance, Abstract Classes and Polymorphism
-------
The purpose of this exercise is to demonstrate 
inheritance and the use of abstract classes to 
specify classes. Polymorphism will then be applied when using the concrete implementations of the abstract class.

Create a class hierarchy that contains several different sensors. They should then address some instances of these sensors and store the readings in different CSV files. An abstract class should contain the specification of all sensors, and concrete implementation classes should then implement the details. A 2-level specification hierarchy will be established.

Tasks


1) **_Specification:_** 

    Create an abstract class Sensor with the following abstract methods:

       getUnit(): This is to return the unit of measurement such as degrees Celsius.

       getValue(): The measurement in the unit, i.e. a double such as 17.0

       getName(): This should only return the name of the measurement, such as "Outside temperature in Baar".

       doMeasurement(): This should actually perform the measurement, i.e. "read out a sensor" for example by addressing a serial interface or making an HTTP request or something else.


2) **_Specification/partial implementation:_**
   
   Create an abstract class for all pressure sensors called PressureSensor. This should extend Sensor.
It should implement the following methods and define the following fields:

       private String unit;

       protected double measurementValue;

       getValue(): simply returns the variable measurementValue.
    
       getUnit(): simply returns the variable unit.


3) **_Implementation:_**

    Create a child class of PressureSensor Barometric1000PressureSensorImpl that implements the following method:
   
        doMeasurement(): This should write a value between 0.5 and 1.05 into the measurmentValue variable. 

    Here, in reality, the physical sensor would be addressed via an interface of the sensor. This would be very manufacturer-specific and could change from model to model of the manufacturer. That means you would have to read the manuals of the manufacturer of Barometric1000 to program this method in reality. But let's make it easy and just write a random value into the variable measurmentValue.

4) **_Implementation_**

    Create a child class of PressureSensor Aqualung2021ProDivePressureSensorImpl and implement the method doMeasurment() which returns values between 0 and 10.0. Again, in reality you would need to read the manuals of Aqualung2021Pro.


5) **_Specification/partial implementation:_**

   Create and partially implement one of the following abstract 
   child classes of Sensor analogous to the class PressureSensor:


         abstract class TemperatureSensor
         abstract class HumiditySensor
         abstract class SpeedSensor

6) **_Implementation:_**

    ** --> ** Implement 2 concrete classes for two models of the abstract class you specified in task 5.


7) **_Using polymorphism:_**

   ** --> ** Now write a class CsvWriter that reads readings from one of these sensors at a given time interval and writes them to a file. (File name, interval and sensor should be configurable).
   - Tip: Create an object of a child class of Sensor and pass it to the constructor of CsvWriter as a parameter together with the interval and the filename.
     File format: each line is Comma Separated Value (CSV) i.e. something like _time stamp_,_name of sensor_,_unit_,_measurement_. Each line corresponds to one measurement.
   - Implement a method "public void run()" which you then call to actually take the measurements and write them to the file.



8) ** --> ** **_Create the CsvWriter in a main class_** and allow the user to select the sensor, the interval and the file name.


9) ** --> ** **_Optional:_** Think about how you would write the same data into a database or into a .json format file.

