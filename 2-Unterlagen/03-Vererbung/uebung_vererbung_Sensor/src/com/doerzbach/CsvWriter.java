package com.doerzbach;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CsvWriter implements Runnable{
    public final Object syncObject =new Object(); // This is only used for multithreading waiting and Notification
    private long interval;
    private String filename;
    private Sensor sensor;
    private boolean running=true;
    private int maxNumberOfMeasurements = -1; // Default is to run for ever
    BufferedWriter bw;
    DateFormat df= new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");


    public CsvWriter(long interval, String filename, Sensor sensor) throws IOException {
        this.interval = interval;
        this.filename = filename;
        this.sensor = sensor;
        bw=new BufferedWriter(new FileWriter(filename,true));
    }
    public CsvWriter(long interval, String filename, Sensor sensor, int maxNumberOfMeasurements) throws IOException {
        this(interval,filename,sensor);
        this.maxNumberOfMeasurements = maxNumberOfMeasurements;
    }
    public void run() {
        while(running&& maxNumberOfMeasurements !=0) {
            if(maxNumberOfMeasurements>0) maxNumberOfMeasurements--;
            sensor.doMeasurement();
            doOutput();
            try {
                synchronized (syncObject) {
                    syncObject.wait(interval); // This is the multithreading variant of waiting interval ms
                }
            } catch (InterruptedException e) {
                System.out.println("Got notified to stop sleeping...");
                running=false;
            }
        }
    }

    private void doOutput()  {
        try {
            bw.write(df.format(new Date()) + "," + sensor.getName() + "," + sensor.getUnit() + "," + sensor.getValue() + "\n");
            bw.flush();
        } catch (IOException iox){
            System.err.println("IOException during in doOutput");
            iox.printStackTrace(System.err);
        }
    }


    public void stop() {
        running=false;
    }
}
