package com.doerzbach;

public abstract class PressureSensor extends Sensor{
    private String unit="Bar";
    protected double measurementValue;
    public double getValue(){
        return measurementValue;
    }
    public String getUnit(){
        return unit;
    }
}
