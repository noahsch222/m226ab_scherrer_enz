# Module M226a und M226b
[TOC]
# M226a - Klassenbasiert (ohne Vererbung) implementieren
[Modulidentifikation M226a](https://www.modulbaukasten.ch/module/226A/4/de-DE?title=Klassenbasiert-(ohne-Vererbung)-implementieren)

## Leistunsbeurteilungen

### M226a LB1 (15%, mündliche Einzelprüfung, 12 min)
Themen: UML, OO-Prinzipien


### M226a LB2 (30%, schriftliche Prüfung, 60 min)
Themen: UML, OO-Prinzipien, ...


### M226a LB3 (55%, praktisches Projekt) 
Bewertungskriterien:<br>
Es müssen alle Elemente im Buch M226 von Ruggerio, Compendio von Kap. 5 bis 11, sowie Kap. 14 und 15 enthalten sein.
- Wer das Minimum des Kap. 13 macht, kann maximal die Note 4.7 erreichen.
- Wer ein eigenes Projekt "gut" abschliesst inkl. "Doku", "JavaDoc" und "JUnit-Tests", kann eine Note 6 machen. |


## Bücher 

[Buch M226_Ruggerio_Compendio](./2-Unterlagen/00-Buecher/Buch__M226_Ruggerio_Compendio/)

## UML-Tools
- https://staruml.io/download
- https://umletino.com

## Fächertagebuch

| Tag  | Auftrag/Übung | Inhalte/Themen |
| ---- | ------------- | ------------------------ |
| 22.08.23 | [A11](./3-Auftraege-Uebungen/A11-Wissensaneignung1.md)            |  Modulvorstellung <br> Installation Eclipse oder ähnliche Programmierumgebung (Buch Kap. 12.1)<br> Beginn mit Buch/Skript Compendio 226 selbständig durchmachen (Teil A (Kap. 1-4))                     |
| 29.08.23 | [A12](./3-Auftraege-Uebungen/A12-WissensaneignungUML.md),[A13](./3-Auftraege-Uebungen/A12-GrundsaetzeDerOOProgrammierung.md)            |  Wissensaufbau zu Objektorientierter Programmierung und Unified Modeling Language (UML)                                       |
| 05.09.23 | [A13](./3-Auftraege-Uebungen/A12-GrundsaetzeDerOOProgrammierung.md)            |  Weiterarbeit am Wissensaufbau, <br>*Input* über UML-Zusammenhänge                                        |
| 12.09.23 |             |  **LB3** Definition eigenes Projektes (max 3 Pers)<br>(Projektumriss, Anforderungsdefinition, Activity-Diagram, Use-cases, ERM?, Class-Diagram, Sequence-Diagram)<br> **LB1** Ab dem 2. Teil des Halbtages laufend Kompetenzabnahmen/Basic-Check (mündlich einzeln, Teil A im Buch)  |
| 19.09.23 ||             |  **LB1** Basic-Check (Fortsetzung) <br>Beginn mit dem eigenen Projekt (Planung/Konzept, UML). Lassen Sie sich von Kap. 13 inspirieren. Bedingung: Es müssen alle Elemente von Kap. 5 bis 11, sowie 14 und 15 enthalten sein.<br><br>Der LP die Aufgabenstellung aufzeigen. Diagramm(e) & Prosa   |
| 26.09.23 |             |  **LB2** Schriftliche Prüfung, 30% ModulNote<br>Weiterarbeit am Projekt                     |
| 03.10.23 |             |  Arbeit am Projekt<br>Präsentierung Zwischenstand des Projektes (v.a. eine Herausforderung)                     |
| 24.10.23 |             |  Arbeit am Projekt<br>Projektbeobachtung durch LP |
| 31.10.23 |             |  Arbeit am Projekt<br>Projektbeobachtung durch LP / erste Projektabnahmen  |
| 07.11.23 |             |  **LB3** Projektabschluss, Projektdemos<br>Projektbesprechung/Notengebung |

# M226b - Objektorientiert (mit Vererbung) implementieren

[Modulidentifikation M226b](https://www.modulbaukasten.ch/module/226B/4/de-DE?title=Objektorientiert-(mit-Vererbung)-implementieren)

## Leistunsbeurteilungen

###  M226b LB1 (30%, schriftliche Prüfung, 60 min)
Themen: UML, OO-Prinzipien, Vererbung, Polymorphismus, JUnit-Tests

###  M226b LB2 (30%, Qualität und Quantität der Übungen)
Themen: Vererbung, Polymorphismus, JUnit-Tests, Anwendung Datenstrukturen & Algorithmen

###  M226b LB3 (40%, Pairprogramming-Miniprojekt)
Thema: Selbstdefinition - Bearbeitungszeit 15-20 Std. (teilweise in Hausarbeit)

## Bücher und Unterlagen
[Buch Java_9_Grundlagen_Programmierung](./2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung/JAV9.pdf)

[Dazugehörige Source Dateien (*.java) JAV9_Arbeitsdateien.zip](2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung/JAV9_Arbeitsdateien.zip)

## Fächertagebuch

| Tag  | Inhalte/Themen, Aufträge/Übungen |
| ---- | -------------------------------  |
| 14.11.23 | fällt aus (LKB)        |
| 21.11.23 | [Präinstruktion 1](1-Prae-Instruktion/M226b-Tag1.md)<br>[Einführung in JUnit-Tests](2-Unterlagen/07-Testen-(JUnit)/)<br>[Dynamisches Binden](https://de.wikipedia.org/wiki/Dynamische_Bindung) <br>Polymorphismus A25 [(.pdf)](3-Auftraege-Uebungen/A25-Inheritance_Polymorphism_Composition.pdf) [(.md)](3-Auftraege-Uebungen/A25-Inheritance_Polymorphism_Composition.md) oder [.docx](2-Unterlagen/04-Dynamische-Bindung-(Polymorphie)/Inheritance_Polymorphism_Composition) |
| 28.11.23 | [Präinstruktion 2](1-Prae-Instruktion/M226b-Tag2.md)<br>Wissensaufbau, Übungen, Training <br>[B22-Wissensaneignung2 (+ 2 Wissenstests)](3-Auftraege-Uebungen/B22-Wissensaneignung2.md)<br>[B23-KlassenAttributeMethoden](3-Auftraege-Uebungen/B23-KlassenAttributeMethoden.md) |
| 05.12.23 | [Präinstruktion 3](1-Prae-Instruktion/M226b-Tag3.md)<br>Wissensaufbau, Übungen, Training <br>[B24-Vererbung](3-Auftraege-Uebungen/B24-Wissensaneignung3Vererbung.md)  |
| 12.12.23 | Wissensaufbau, Übungen, Training <br>[Auswahl einer Aufgabenstellung](4-Aufgabenstellungen) |
| ---- | ---- Weihnachtsferien ----       |
| 19.12.23 | **LB1**, Start **LB3** --> [Miniprojekt-Beispiele](./4-Aufgabenstellungen/Miniprojekt-Beispiele)|
| 09.01.24 | Arbeit an LB3          |
| 16.01.24 | Arbeit an LB3          |
| 23.01.24 | Arbeit an LB3          |
| 30.01.24 | Abgabe **LB3**         |
