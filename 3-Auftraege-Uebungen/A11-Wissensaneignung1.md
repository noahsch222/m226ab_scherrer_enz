# 1 - Wissensaneignung
(Buch Kap.1-4, M. Ruggerio, Compendio)

Zeitbedarf: 3-6 Std
 - Installieren Sie eine IDE wie [Eclipse](https://www.eclipse.org/downloads/) oder [IntelliJ Community Edition](https://www.jetbrains.com/de-de/idea/download/) (wenn Sie das noch nicht schon haben)
 - Erstellt ein Git-Repo und ladet mich ein dazu. Meine ID in gitlab und github ist `armindoerzbachtbz`
 - Warm-UP: 
    1) Erstellen sie ein Programm welches einen Input ueber das Terminal entgegen nimmt und diesen wieder ausgibt
    2) Erweitern sie dieses Programm so, dass es solange einen Input entegeben nimmt, bis sie `Stop` eingeben. Dann soll das  Programm enden.
    3) Erweitern sie das Programm so, dass es maximal 10 mal einen Input entgegennimmt und am Schluss alle Inputs ausgibt. Falls `Stop` vorher eingegeben wird, soll das Programm alle Inputs ausgeben und sich sofort beenden.
    [**Muster-Lösungen**](./A11_Loesungen/src/com/doerzbach/)
 - Lesen Sie Kap. 1 bis Kap. 4 (S. 50) im Buch__M226_Compendio_LehrbuchTutorialAufgabenstellungen.pdf
 - Bauen Sie die vorkommenden Codebeispiele bei sich in Ihrem System nach
 - Zeichnen Sie die Diagramme in https://draw.io nach und speichern Sie sie in Ihrem OneDrive (und/oder ePortfolio). Wenn Sie eigene Erweiterungen machen, führen Sie sie in Ihren Diagrammen nach.

Legen sie den Code und die Diagramme als .java und .png unter 'A11/Compendio' in ihrem Git-Repo ab.


Weitere Informationen zu UML kommen noch.
Über die Grundlagen der Objektorientierung und UML-Diagramme wird es eine mündliche und eine schriftliche Prüfung geben

<https://draw.io>

[Ruggerio, Compendio](../2-Unterlagen/00-Buecher/Buch__M226_Ruggerio_Compendio)