# Wissensaneignung UML 


**Zeitbedarf** ca. 4 Lektionen
das Thema ist prüfungsrelevant

[TOC]

## Warum brauchen wir UML?

__ChatGPT sagt dazu:__

Die wesentlichen Zwecke von UML sind:

- Kommunikation: UML erleichtert die effektive Kommunikation zwischen verschiedenen Stakeholdern in einem Projekt.
- Modellierung: UML ermöglicht die Darstellung und Visualisierung komplexer Systeme durch Modelle.
- Dokumentation: UML unterstützt die Erstellung von systembezogener Dokumentation.
- Analyse und Design: UML hilft bei der Analyse von Anforderungen und beim Entwurf von Systemen.
- Tests und Validierung: UML kann zur Erstellung von Testfällen und zur Validierung des Systems verwendet werden.

Diese Zwecke bilden die Kernfunktionen von UML in der Softwareentwicklung und anderen technischen Bereichen.

__Ich würde sagen:__
Die wesentlichen Zweck von UML sind:

- Kommunikation: UML erleichtert die effektive Kommunikation zwischen Stakeholdern, die nicht dieselben Kenntnisse haben.

- Analyse und Design: UML hilft bei der Analyse von Anforderungen und beim Entwurf von Systemen, weil man die Anforderungen mit Informatikfremden Personen aufnehmen muss und den Entwurf mit denselben Personen besprechen muss.

Den Rest könnt ihr vergessen.

## Lernen sie UML anhand Beispiels Flexibus

Gruppenarbeit: 3-4er Gruppen, keiner arbeitet alleine.

Erstellen sie als erstes einen Abgabeordner in ihrem Repo mit dem Namen Flexibus, dort legen sie die erstellten Diagramme als PNG ab.

Die Firma Flexibus hat eine Flotte von 10 Busen beschäftigt Disponenten und Fahrer. Sie ist schnell gewachsen. Bis jetzt haben 2 Disponenten jeweils das Planen der Fahrten übernommen und die Fahrer eingeteilt. Das geschah mit folgendem [tourenplaner-Excel-Sheet](./media/tourenplaner.xlsx).
Folgende Vorgänge sind vorgesehen:
- Die Disponenten planen und organisieren die Fahrten. 
- Eine Fahrt hat mehrere Stationen, wobei Orte nur einmal angefahren werden dürfen. 
- Jeder Fahrt wird jeweils ein Fahrzeug zugeordnet, welches eine gewisse Anzahl Sitzplätze zur Verfügung stellt. 
- An jeder Station kann der Fahrer wechseln. 
- Pro Station wird eine Ankunfts- und eine Abfahrtszeit erfasst werden, wobei für die Start-Station nur Abfahrtszeit erfasst wird und für die Ziel-Station nur die Ankunftszeit.
- Fahrer und Disponenten sind über Telefon erreichbar. 
- Die Verkauf der Tickets für die mitfahrenden Kunden geschieht bei einer externen Firma.

In der Zwischenzeit gibt es Probleme im Backoffice, weil die Planung mit mehr Bussen und Fahrern immer komplexer wird. 

Darum hat die Firma Flexibus ihnen den Auftrag geben die Situation zu Analysieren und einen Vorschlag für eine Software-Lösung zu machen. 

### Erste Aufgabe: Sie erstellen ein Use-Case-Diagramm

Um sicherzustellen, dass sie das Problem richtig verstanden haben erstellen sie ein UML-Use-Case-Diagramm.

**Wie sieht ein soches UML-Use-Case-Diagramm aus?**  
[Lucidchart Video-Tutorial zu Use-Case-Diagramm (13:23 min)](https://www.youtube.com/watch?v=zid-MVo7M-E)

Benutzen sie [draw.io](https://draw.io) um ein erstes Use-Case-Diagramm zu erstellen. Um die richtigen Shapes zu verwenden, benutzen "Search Shapes" und geben `UML Actor` und `UML Use Case` ein.

Kommen sie zu mir und diskutieren ihr Diagramm mit mir. Dabei wird ihnen dann klarer, ob sie die Firma Flexibus nicht richtig verstanden haben.

### Zweite Aufgabe: Sie erstellen ein UML-Class-Diagramm

Jetzt wo sie *scheinbar* verstanden haben um was es bei der Firma Flexibus geht, werden sie alle Objekte identifizieren welche es braucht um die bedürfnisse der Firma abzubilden.

**Wie sieht ein UML-Klassen-Diagramm aus?**
[Lucidchart Video-Tutorial zu Klassendiagramm (10:16 min) ](https://www.youtube.com/watch?v=UI6lqHOVHic)

Erstellen sie zuerst nur ein Use-Case-Diagramm mit den Klassennamen und erklären sie mir dieses als Auftraggeber.

Erst jetzt verfeinern sie diese und ergänzen es mit den wichtigsten Attributen und Methoden.

### Dritte Aufgabe: Sie erstellen ein UML-Sequenzdiagramm

Beim diskutieren der Klassen- und Use-Case-Diagramme wird ihnen aufgefallen sein, dass gewisse Use-Cases kompliziertere Vorgänge beschreiben, bei welchen mehrere Klassen involviert sind. Genau zu einem solchen sollen sie jetzt ein Sequenz-Diagramm zeichnen.

**Wie sieht ein UML-Sequenz-Diagramm aus?**
[Lucidchart Video-Tutorial zu Sequenz-Diagramm (8:37 min) ](https://www.youtube.com/watch?v=pCK6prSq8aw)  

Kommen sie wieder zu mir als Auftraggeber um ihre Lösung zu diskutieren.

Seien sie nicht erstaunt wenn sie die Klassendiagramme und vielleicht auch die Use-Case-Diagramme nochmals anpassen, um einfachere Vorgänge zu bekommen.


